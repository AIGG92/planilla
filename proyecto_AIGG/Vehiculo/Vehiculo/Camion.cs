﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehiculo
{
    class Camion_Carga : Vehiculo_01
    {
        int capacidad_carga;
        int size_contenedor;

        public Camion_Carga(String Marca, String Modelo, int Motor,int capacidad_carga, int size_contenedor) : base(Marca, Modelo, Motor)
        {
            this.capacidad_carga = capacidad_carga;
            this.size_contenedor = size_contenedor;

        }

        public void camion_carg(int cargar_camion)
        {
            capacidad_carga = cargar_camion;
        }
        public String dime_carga()
        {
            return "Capacidad de carga " + capacidad_carga +" Toneladas";
        } 

        public void tam_contenedor(int contenedor_t)
        {
            size_contenedor = contenedor_t;
        }

        public String dime_contenedor()
        {
            return "Tamano de contenedor: " + size_contenedor+ " Metros";
        }
        
           
    }
}
