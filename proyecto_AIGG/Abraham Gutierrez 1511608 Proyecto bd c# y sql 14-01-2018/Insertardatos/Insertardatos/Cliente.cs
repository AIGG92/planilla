﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Npgsql;
using NpgsqlTypes;

namespace Insertardatos
{
    class Cliente
    {
        SqlConnection cn = new SqlConnection(@"Data Source=ABRAHAM_GUTIERR\SQLAIGG;Initial Catalog=insertardatos;Integrated Security=True");

        //
        string con = "server-localhost; port-5432; user id-postgres; passwoer-123456789; Database-insertardatos";

        DataSet datos = new DataSet();
      NpgsqlConnection conn = new NpgsqlConnection();
      //  

        public String Nombre { get; set; }

        public String Apellidos { get; set; }

        public String Edad { get; set; }

        public String Ciudad { get; set; }

        // Codigo de conexion de postgre 
        internal NpgsqlConnection Conn 
        {
            get
            {
                return conn;
            }

            set
            {
                conn = value;
            }
        }

        

        public Cliente()
        {

        }

        public Cliente(String nombre, String apellido, String edad, String ciudad)
        {
            this.Nombre = nombre;
            this.Apellidos = apellido;
            this.Edad = edad;
            this.Ciudad = ciudad;
         
        }

        public bool RegistrarDatos()
        {
            try
            {
                var Consulta = "insert into cliente(nombre,apellido,edad,ciudad)";
                Consulta += "values(@nombre,@apellido,@edad,@ciudad)";
                var cmd = new SqlCommand(Consulta, cn);
              
                cmd.Parameters.AddWithValue("@nombre",this.Nombre);
                cmd.Parameters.AddWithValue("@apellido", this.Apellidos);
                cmd.Parameters.AddWithValue("@edad", this.Edad);
                cmd.Parameters.AddWithValue("@ciudad", this.Ciudad);
               
                cmd.CommandType = CommandType.Text;
                cn.Open();
                int r = cmd.ExecuteNonQuery();
                cn.Close();

                if (r == 1)
                {
                    return true;
                }
            }
            catch (SqlException ex)
            {
              // System.Windows.Forms.Message.Show(ex.Message);
                return false;

            }

            return false;
        }



    }


    class Padres :Cliente
    {
        SqlConnection cn = new SqlConnection(@"Data Source=ABRAHAM_GUTIERR\SQLAIGG;Initial Catalog=insertardatos;Integrated Security=True");
        //
        string con = "server-localhost; port-5432; user id-postgres; passwoer-123456789; Database-insertardatos";

        DataSet datos = new DataSet();
        NpgsqlConnection conn = new NpgsqlConnection();
        //  
        public String Mama { get; set; }

        public String Papa { get; set; }
     


        public Padres(String mom, String dad)
        {
            
            this.Mama = mom;
            this.Papa = dad;
           
        }
        public bool RegistrarDatos2()
        {
            try
            {
                var Consulta = "insert into Padres(papa,mama)";
                Consulta += "values(@papa,@mama)";
                var cmd = new SqlCommand(Consulta, cn);

                cmd.Parameters.AddWithValue("@papa", this.Papa);
                cmd.Parameters.AddWithValue("@mama", this.Mama);
                cmd.CommandType = CommandType.Text;
                cn.Open();
                int r = cmd.ExecuteNonQuery();
                cn.Close();

                if (r == 1)
                {
                    return true;
                }
            }
            catch (SqlException ex)
            {
                // System.Windows.Forms.Message.Show(ex.Message);
                return false;

            }

            return false;
        }

    }

}
